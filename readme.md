### You must install Python 3.X to use the [installer](https://gitea.com/kckarnige/chs_modpack/releases) and updater

[Microsoft Store Listing](https://www.microsoft.com/en-us/p/python-39/9p7qfqmjrfp7)

[Self-hosted Windows Package (From Microsoft Store)](https://gitea.com/kckarnige/python_winpack/raw/branch/master/python3.9.msix)

[Python Website](https://www.python.org)